"use strict";

const State = {
    VACANT: 0, BLACK_MARKER: 1, WHITE_MARKER: 2, BLACK_RING: 3, WHITE_RING: 4,
    BLACK_MARKER_RING: 5, WHITE_MARKER_RING: 6
};

export default State;
