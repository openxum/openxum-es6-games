"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import GameType from './game_type.mjs';
import Intersection from './intersection.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';
import State from './state.mjs';

// grid constants definition
const begin_letter = ['B', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'D', 'E', 'G'];
const end_letter = ['E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K', 'K', 'J'];
const begin_number = [2, 1, 1, 1, 1, 2, 2, 3, 4, 5, 7];
const end_number = [5, 7, 8, 9, 10, 10, 11, 11, 11, 11, 10];
const begin_diagonal_letter = ['B', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'D', 'E', 'G'];
const end_diagonal_letter = ['E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K', 'K', 'J'];
const begin_diagonal_number = [7, 5, 4, 3, 2, 2, 1, 1, 1, 1, 2];
const end_diagonal_number = [10, 11, 11, 11, 11, 10, 10, 9, 8, 7, 5];

// enums definition
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];

class Engine extends OpenXum.Engine {
    constructor(t, c) {
        super();
        this._type = t;
        this._current_color = c;
        this._marker_number = 51;
        this._placed_black_ring_coordinates = [];
        this._placed_white_ring_coordinates = [];
        this._removed_black_ring_number = 0;
        this._removed_white_ring_number = 0;
        this._last_marker = null;
        this._intersections = {};
        this._phase = Phase.PUT_RING;

        for (let i = 0; i < letters.length; ++i) {
            const l = letters[i];

            for (let n = begin_number[l.charCodeAt(0) - 'A'.charCodeAt(0)];
                 n <= end_number[l.charCodeAt(0) - 'A'.charCodeAt(0)]; ++n) {
                const coordinates = new Coordinates(l, n);

                this._intersections[coordinates.hash()] = new Intersection(coordinates);
            }
        }
    }

// public methods
    build_move() {
        return new Move();
    }

    available_marker_number() {
        return this._marker_number;
    }

    clone() {
        let o = new Engine(this._type, this._current_color);

        o._set(this._phase, this._marker_number, this._placed_black_ring_coordinates, this._placed_white_ring_coordinates,
            this._removed_black_ring_number, this._removed_white_ring_number, this._intersections);
        return o;

    }

    current_color() {
        return this._current_color;
    }

    exist_intersection(letter, number) {
        const coordinates = (new Coordinates(letter, number)).hash();

        return coordinates in this._intersections;
    }

    get_free_intersections() {
        let list = [];

        for (let i = 0; i < letters.length; ++i) {
            const l = letters[i];

            for (let n = begin_number[l.charCodeAt(0) - 'A'.charCodeAt(0)];
                 n <= end_number[l.charCodeAt(0) - 'A'.charCodeAt(0)]; ++n) {
                const coordinates = new Coordinates(l, n);

                if (this._intersections[coordinates.hash()].state() === State.VACANT) {
                    list.push(coordinates);
                }
            }
        }
        return list;
    }

    get_placed_ring_coordinates(color) {
        return (color === Color.BLACK) ? this._placed_black_ring_coordinates :
            this._placed_white_ring_coordinates;
    }

    get_name() {
        return 'yinsh';
    }

    get_possible_move_list() {
        let list = [];

        if (this._phase === Phase.PUT_RING) {
            const intersections = this.get_free_intersections();

            intersections.forEach((e) => {
                list.push(new Move(MoveType.PUT_RING, this._current_color, e))
            });
        } else if (this._phase === Phase.PUT_MARKER) {
            Object.keys(this._intersections).map((key, index) => {
                const intersection = this._intersections[key];

                if (this._current_color === Color.BLACK && intersection.state() === State.BLACK_RING ||
                    this._current_color === Color.WHITE && intersection.state() === State.WHITE_RING) {
                    if (this.get_possible_moving_list(intersection.coordinates(), this._current_color, false).length > 0) {
                        list.push(new Move(MoveType.PUT_MARKER, this._current_color, intersection.coordinates()));
                    }
                }
            });
        } else if (this._phase === Phase.MOVE_RING) {
            const coordinates = this.get_possible_moving_list(this._last_marker, this._current_color, true);

            coordinates.forEach((e) => {
                list.push(new Move(MoveType.MOVE_RING, this._current_color, this._last_marker, e))
            });
        } else if (this._phase === Phase.REMOVE_RING_BEFORE || this._phase === Phase.REMOVE_RING_AFTER) {
            const rings = this._current_color === Color.BLACK ? this._placed_black_ring_coordinates : this._placed_white_ring_coordinates;

            rings.forEach((r) => {
                list.push(new Move(MoveType.REMOVE_RING, this._current_color, r));
            });
        } else if (this._phase === Phase.REMOVE_ROWS_BEFORE || this._phase === Phase.REMOVE_ROWS_AFTER) {
            const rows = this.get_rows(this._current_color);

            rows.forEach((r) => {
                if (r.length === 5) {
                    list.push(new Move(MoveType.REMOVE_ROW, this._current_color, r));
                } else {
                    for (let index = 0; index < r.length - 5; ++index) {
                        let row = [];

                        for (let i = index; i < index + 5; ++i) {
                            row.push(r[i]);
                        }
                        list.push(new Move(MoveType.REMOVE_ROW, this._current_color, row));
                    }
                }
            });
        }
        return list;
    }

    get_possible_moving_list(origin, color, control) {
        let list = [];

        if (Object.keys(this._intersections).indexOf(origin.hash().toString()) === -1) {
            return list;
        }

        if (control &&
            !((this._intersections[origin.hash()].state() === State.BLACK_MARKER_RING && color === Color.BLACK) ||
                (this._intersections[origin.hash()].state() === State.WHITE_MARKER_RING && color === Color.WHITE))) {
            return list;
        }

        let state = {
            ok: true,
            no_vacant: false
        };
        const letter = this._intersections[origin.hash()].letter();
        const letter_index = this._intersections[origin.hash()].letter().charCodeAt(0) - 'A'.charCodeAt(0);
        const number = this._intersections[origin.hash()].number();
        let n;
        let l;

        // letter + number increase
        {
            n = number + 1;
            state.ok = true;
            state.no_vacant = false;
            while (n <= end_number[letter.charCodeAt(0) - 'A'.charCodeAt(0)] && state.ok) {
                state = this._verify_intersection(letter, n, state);
                if ((state.ok && !state.no_vacant) || (!state.ok && state.no_vacant)) {
                    list.push(this._get_intersection(letter, n).coordinates());
                }
                ++n;
            }
        }
        // letter + number decrease
        {
            n = number - 1;
            state.ok = true;
            state.no_vacant = false;
            while (n >= begin_number[letter.charCodeAt(0) - 'A'.charCodeAt(0)] && state.ok) {
                state = this._verify_intersection(letter, n, state);
                if ((state.ok && !state.no_vacant) || (!state.ok && state.no_vacant)) {
                    list.push(this._get_intersection(letter, n).coordinates());
                }
                --n;
            }
        }
        // number + letter increase
        {
            l = letter_index + 1;
            state.ok = true;
            state.no_vacant = false;
            while (l <= (end_letter[number - 1].charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                state = this._verify_intersection(letters[l], number, state);
                if ((state.ok && !state.no_vacant) || (!state.ok && state.no_vacant)) {
                    list.push(this._get_intersection(letters[l], number).coordinates());
                }
                ++l;
            }
        }
        // number + letter decrease
        {
            l = letter_index - 1;
            state.ok = true;
            state.no_vacant = false;
            while (l >= (begin_letter[number - 1].charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                state = this._verify_intersection(letters[l], number, state);
                if ((state.ok && !state.no_vacant) || (!state.ok && state.no_vacant)) {
                    list.push(this._get_intersection(letters[l], number).coordinates());
                }
                --l;
            }
        }
        // number increase + letter increase
        {
            n = number + 1;
            l = letter_index + 1;
            state.ok = true;
            state.no_vacant = false;
            while (n <= end_number[l] &&
            l <= (end_letter[n - 1].charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                state = this._verify_intersection(letters[l], n, state);
                if ((state.ok && !state.no_vacant) || (!state.ok && state.no_vacant)) {
                    list.push(this._get_intersection(letters[l], n).coordinates());
                }
                ++l;
                ++n;
            }
        }
        // number decrease + letter decrease
        {
            n = number - 1;
            l = letter_index - 1;
            state.ok = true;
            state.no_vacant = false;
            while (n >= begin_number[l] &&
            l >= (begin_letter[n - 1].charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                state = this._verify_intersection(letters[l], n, state);
                if ((state.ok && !state.no_vacant) || (!state.ok && state.no_vacant)) {
                    list.push(this._get_intersection(letters[l], n).coordinates());
                }
                --l;
                --n;
            }
        }
        return list;
    }

    get_rows(color) {
        const state = (color === Color.BLACK) ? State.BLACK_MARKER : State.WHITE_MARKER;
        let result = {
            start: false,
            row: [],
            rows: []
        };

        for (let n = 1; n <= 11; ++n) {
            result.start = false;
            result.row = [];
            for (let l = begin_letter[n - 1].charCodeAt(0); l <= end_letter[n - 1].charCodeAt(0); ++l) {
                result = this._build_row(letters[l - 'A'.charCodeAt(0)], n, state, result);
            }
            if (result.row.length >= 5) {
                result.rows.push(result.row);
            }
        }

        for (let l = 'A'.charCodeAt(0); l < 'L'.charCodeAt(0); ++l) {
            result.start = false;
            result.row = [];
            for (let n = begin_number[l - 'A'.charCodeAt(0)]; n <= end_number[l - 'A'.charCodeAt(0)]; ++n) {
                result = this._build_row(letters[l - 'A'.charCodeAt(0)], n, state, result);
            }
            if (result.row.length >= 5) {
                result.rows.push(result.row);
            }
        }

        for (let i = 0; i < 11; ++i) {
            let n = begin_diagonal_number[i];
            let l = begin_diagonal_letter[i].charCodeAt(0);

            result.start = false;
            result.row = [];
            while (l <= end_diagonal_letter[i].charCodeAt(0) &&
            n <= end_diagonal_number[i]) {
                result = this._build_row(letters[l - 'A'.charCodeAt(0)], n, state, result);
                ++l;
                ++n;
            }
            if (result.row.length >= 5) {
                result.rows.push(result.row);
            }
        }
        return result.rows;
    }

    get_type() {
        return this._type;
    }

    intersection_state(letter, number) {
        return this._get_intersection(letter, number).state();
    }

    intersections() {
        return this._intersections;
    }

    is_blitz() {
        return this._type === GameType.BLITZ;
    }

    is_finished() {
        if (this._type === GameType.BLITZ) {
            return this._removed_black_ring_number === 1 || this._removed_white_ring_number === 1 || this._marker_number === 0;
        } else { // type = REGULAR
            return this._removed_black_ring_number === 3 || this._removed_white_ring_number === 3 || this._marker_number === 0;
        }
    }

    is_initialized() {
        return this._placed_black_ring_coordinates.length === 5 &&
            this._placed_white_ring_coordinates.length === 5;
    };

    is_regular() {
        return this.type === GameType.REGULAR;
    }

    move(move) {
        if (move.type() === MoveType.PUT_RING) {
            this._put_ring(move.coordinates(), this._current_color);
        } else if (move.type() === MoveType.PUT_MARKER) {
            this._put_marker(move.coordinates(), this._current_color);
        } else if (move.type() === MoveType.REMOVE_RING) {
            this._remove_ring(move.coordinates(), this._current_color);
        } else if (move.type() === MoveType.MOVE_RING) {
            this._move_ring(move.from(), move.to());
        } else if (move.type() === MoveType.REMOVE_ROW) {
            this._remove_row(move.row(), this._current_color);
        }
    }

    parse(str) {
        // TODO
    }

    phase() {
        return this._phase;
    }

    to_string() {
        // TODO
    }

    winner_is() {
        if (this.is_finished()) {
            if (this._type === GameType.REGULAR) {
                if (this._removed_black_ring_number === 3 ||
                    this._removed_black_ring_number > this._removed_white_ring_number) {
                    return Color.BLACK;
                } else if (this._removed_white_ring_number === 3 ||
                    this._removed_black_ring_number < this._removed_white_ring_number) {
                    return Color.WHITE;
                } else {
                    return Color.NONE;
                }
            } else {
                if (this._removed_black_ring_number === 1) {
                    return Color.BLACK;
                } else if (this._removed_white_ring_number === 1) {
                    return Color.WHITE;
                } else {
                    return Color.NONE;
                }
            }
        }
        return false;
    }

// private methods
    _build_row(letter, number, state, previous) {
        let result = previous;
        const coordinates = new Coordinates(letter, number);
        const intersection = this._intersections[coordinates.hash()];

        if (!result.start && intersection.state() === state) {
            result.start = true;
            result.row.push(coordinates);
        } else if (result.start && intersection.state() === state) {
            result.row.push(coordinates);
        } else if (result.start && intersection.state() !== state) {
            if (result.row.length >= 5) {
                result.rows.push(result.row);
            }
            result.start = false;
            result.row = [];
        }
        return result;
    }

    _change_color() {
        this._current_color = this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK;
    }

    _flip(letter, number) {
        const coordinates = (new Coordinates(letter, number)).hash();

        if (coordinates in this._intersections) {
            this._intersections[coordinates].flip();
        }
    }

    _flip_row(origin, destination) {
        let n;
        let l;

        if (origin.letter() === destination.letter()) {
            if (origin.number() < destination.number()) {
                n = origin.number() + 1;
                while (n < destination.number()) {
                    this._flip(origin.letter(), n);
                    ++n;
                }
            } else {
                n = origin.number() - 1;
                while (n > destination.number()) {
                    this._flip(origin.letter(), n);
                    --n;
                }
            }
        } else if (origin.number() === destination.number()) {
            if (origin.letter() < destination.letter()) {
                l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) + 1;
                while (l < destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) {
                    this._flip(letters[l], origin.number());
                    ++l;
                }
            } else {
                l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) - 1;
                while (l > destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) {
                    this._flip(letters[l], origin.number());
                    --l;
                }
            }
        } else {
            if (origin.letter() < destination.letter()) {
                n = origin.number() + 1;
                l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) + 1;
                while (l < destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) {
                    this._flip(letters[l], n);
                    ++l;
                    ++n;
                }
            } else {
                n = origin.number() - 1;
                l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) - 1;
                while (l > destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) {
                    this._flip(letters[l], n);
                    --l;
                    --n;
                }
            }
        }
    }

    _get_intersection(letter, number) {
        const coordinates = (new Coordinates(letter, number)).hash();

        if (coordinates in this._intersections) {
            return this._intersections[coordinates];
        } else {
            return null;
        }
    }

    _move_ring(origin, destination) {
        if (Object.keys(this._intersections).indexOf(origin.hash().toString()) === -1) {
            return false;
        }
        if (Object.keys(this._intersections).indexOf(destination.hash().toString()) === -1) {
            return false;
        }
        if (this._intersections[destination.hash()].state() !== State.VACANT) {
            return false;
        }
        if (!this._verify_moving(origin, destination)) {
            return false;
        }

        const intersection_origin = this._intersections[origin.hash()];
        const intersection_destination = this._intersections[destination.hash()];
        const color = intersection_origin.color();

        intersection_origin.remove_ring();
        intersection_destination.put_ring(color);
        this._flip_row(origin, destination);

        if (color === Color.BLACK) {
            this._remove_black_ring(origin);
            this._placed_black_ring_coordinates.push(destination);
        } else {
            this._remove_white_ring(origin);
            this._placed_white_ring_coordinates.push(destination);
        }
        if (this.get_rows(this._current_color).length > 0) {
            this._phase = Phase.REMOVE_ROWS_AFTER;
        } else {
            this._change_color();
            if (this.get_rows(this._current_color).length === 0) {
                this._phase = Phase.PUT_MARKER;
            } else {
                this._phase = Phase.REMOVE_ROWS_BEFORE;
            }
        }
        return true;
    }

    _put_marker(coordinates, color) {
        if (this._marker_number > 0) {
            if (Object.keys(this._intersections).indexOf(coordinates.hash().toString()) !== -1) {
                this._intersections[coordinates.hash()].put_marker(color);
                this._last_marker = coordinates;
            } else {
                return false;
            }
            --this._marker_number;
        } else {
            return false;
        }
        this._phase = Phase.MOVE_RING;
        return true;
    }

    _put_ring(coordinates, color) {
        if (color !== this._current_color) {
            return false;
        }
        if ((color === Color.BLACK && this._placed_black_ring_coordinates.length === 5) ||
            (color === Color.WHITE && this._placed_white_ring_coordinates.length === 5)) {
            return false;
        }
        if (coordinates.hash() in this._intersections) {
            this._intersections[coordinates.hash()].put_ring(color);
        } else {
            return false;
        }
        if (color === Color.WHITE) {
            this._placed_white_ring_coordinates.push(coordinates);
        } else {
            this._placed_black_ring_coordinates.push(coordinates);
        }
        if (this._placed_black_ring_coordinates.length === 5 &&
            this._placed_white_ring_coordinates.length === 5) {
            this._phase = Phase.PUT_MARKER;
        }
        this._change_color();
        return true;
    }

    _remove_ring(coordinates, color) {
        const intersection = this._intersections[coordinates.hash()];

        if (intersection.color() === color) {
            intersection.remove_ring_board();
        } else {
            return false;
        }

        if (color === Color.BLACK) {
            this._remove_black_ring(coordinates);
            ++this._removed_black_ring_number;
        } else {
            this._remove_white_ring(coordinates);
            ++this._removed_white_ring_number;
        }
        if (this.is_finished()) {
            this._phase = Phase.FINISH;
        } else {
            if (this._phase === Phase.REMOVE_RING_AFTER) {
                if (this.get_rows(this._current_color).length === 0) {
                    this._change_color();
                    if (this.get_rows(this._current_color).length === 0) {
                        this._phase = Phase.PUT_MARKER;
                    } else {
                        this._phase = Phase.REMOVE_ROWS_BEFORE;
                    }
                } else {
                    this._phase = Phase.REMOVE_ROWS_AFTER;
                }
            } else { // phase === Yinsh.Phase.REMOVE_RING_BEFORE
                if (this.get_rows(this._current_color).length === 0) {
                    this._phase = Phase.PUT_MARKER;
                } else {
                    this._phase = Phase.REMOVE_ROWS_BEFORE;
                }
            }
        }
        return true;
    }

    _remove_black_ring(coordinates) {
        let list = [];
        let i = 0;

        while (i < this._placed_black_ring_coordinates.length) {
            if (this._placed_black_ring_coordinates[i].hash() !== coordinates.hash()) {
                list.push(this._placed_black_ring_coordinates[i]);
            }
            ++i;
        }
        this._placed_black_ring_coordinates = list;
    }

    _remove_marker(letter, number) {
        const coordinates = (new Coordinates(letter, number)).hash();

        if (coordinates in this._intersections) {
            this._intersections[coordinates].remove_marker();
            ++this._marker_number;
        }
    }

    _remove_row(row, color) {
        if (row.length !== 5) {
            return false;
        }
        for (let j = 0; j < row.length; ++j) {
            this._remove_marker(row[j].letter(), row[j].number());
        }
        if (this._phase === Phase.REMOVE_ROWS_AFTER) {
            this._phase = Phase.REMOVE_RING_AFTER;
        } else {
            this._phase = Phase.REMOVE_RING_BEFORE;
        }
        return true;
    }

    _remove_white_ring(coordinates) {
        let list = [];
        let i = 0;

        while (i < this._placed_white_ring_coordinates.length) {
            if (this._placed_white_ring_coordinates[i].hash() !== coordinates.hash()) {
                list.push(this._placed_white_ring_coordinates[i]);
            }
            ++i;
        }
        this._placed_white_ring_coordinates = list;
    }

    _removed_ring_number(color) {
        return (color === Color.BLACK) ? this._removed_black_ring_number :
            this._removed_white_ring_number;
    }

    _set(phase, marker_number, placed_black_ring_coordinates, placed_white_ring_coordinates, removed_black_ring_number, removed_white_ring_number, intersections) {
        Object.keys(intersections).map((key, index) => {
            this._intersections[key] = intersections[key].clone();
        });
        this._phase = phase;
        this._marker_number = marker_number;
        for (let index = 0; index < placed_black_ring_coordinates.length; ++index) {
            this._placed_black_ring_coordinates[index] = placed_black_ring_coordinates[index].clone;
        }
        for (let index = 0; index < placed_white_ring_coordinates.length; ++index) {
            this._placed_white_ring_coordinates[index] = placed_white_ring_coordinates[index].clone;
        }
        this._removed_black_ring_number = removed_black_ring_number;
        this._removed_white_ring_number = removed_white_ring_number;
    }

    _verify_intersection(letter, number, result) {
        const coordinates = (new Coordinates(letter, number)).hash();

        if (coordinates in this._intersections) {
            const state = this._intersections[coordinates].state();

            if (state === State.BLACK_RING || state === State.WHITE_RING) {
                result.no_vacant = false; // if ring is presenter after row of markers
                result.ok = false;
            } else if (state === State.BLACK_MARKER || state === State.WHITE_MARKER) {
                result.no_vacant = true;
            } else if (state === State.VACANT && result.no_vacant) {
                result.ok = false;
            }
        }
        return result;
    }

    _verify_moving(origin, destination) {
        let state = {
            ok: true,
            no_vacant: false
        };
        let n;
        let l;

        if (this._intersections[origin.hash()].state() !== State.BLACK_MARKER_RING &&
            this._intersections[origin.hash()].state() !== State.WHITE_MARKER_RING) {
            return false;
        }

        if (origin.hash() === destination.hash() ||
            this._intersections[destination.hash()].state() !== State.VACANT) {
            return false;
        }

        if (origin.letter() === destination.letter()) {
            if (origin.number() < destination.number()) {
                n = origin.number() + 1;
                state.no_vacant = false;
                while (n < destination.number() && state.ok) {
                    state = this._verify_intersection(origin.letter(), n, state);
                    ++n;
                }
            } else {
                n = origin.number() - 1;
                state.no_vacant = false;
                while (n > destination.number() && state.ok) {
                    state = this._verify_intersection(origin.letter(), n, state);
                    --n;
                }
            }
        } else if (origin.number() === destination.number()) {
            if (origin.letter().charCodeAt(0) < destination.letter().charCodeAt(0)) {
                l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) + 1;
                state.no_vacant = false;
                while (l < (destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                    state = this._verify_intersection(letters[l], origin.number(), state);
                    ++l;
                }
            } else {
                l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) - 1;
                state.no_vacant = false;

                while (l > (destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                    state = this._verify_intersection(letters[l], origin.number(), state);
                    --l;
                }
            }
        } else {
            if (origin.letter().charCodeAt(0) - destination.letter().charCodeAt(0) ===
                origin.number() - destination.number()) {
                if (origin.letter().charCodeAt(0) < destination.letter().charCodeAt(0)) {
                    n = origin.number() + 1;
                    l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) + 1;
                    state.no_vacant = false;
                    while (l < (destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                        state = this._verify_intersection(letters[l], n, state);
                        ++l;
                        ++n;
                    }
                } else {
                    n = origin.number() - 1;
                    l = (origin.letter().charCodeAt(0) - 'A'.charCodeAt(0)) - 1;
                    state.no_vacant = false;
                    while (l > (destination.letter().charCodeAt(0) - 'A'.charCodeAt(0)) && state.ok) {
                        state = this._verify_intersection(letters[l], n, state);
                        --l;
                        --n;
                    }
                }
            } else {
                state.ok = false;
            }
        }
        return state.ok;
    }
}

export default Engine;
