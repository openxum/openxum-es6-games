"use strict";

class Coordinates {
    constructor(l, n) {
        this._letter = l;
        this._number = n;
    }

// public methods
    clone() {
        return new Coordinates(this._letter, this._number);
    }

    hash() {
        return (this._letter.charCodeAt(0) - 'A'.charCodeAt(0)) + (this._number - 1) * 11;
    }

    is_valid() {
        return (this._letter === 'A' && this._number >= 2 && this._number <= 5) ||
            (this._letter === 'B' && this._number >= 1 && this._number <= 7) ||
            (this._letter === 'C' && this._number >= 1 && this._number <= 8) ||
            (this._letter === 'D' && this._number >= 1 && this._number <= 9) ||
            (this._letter === 'E' && this._number >= 1 && this._number <= 10) ||
            (this._letter === 'F' && this._number >= 2 && this._number <= 10) ||
            (this._letter === 'G' && this._number >= 2 && this._number <= 11) ||
            (this._letter === 'H' && this._number >= 3 && this._number <= 11) ||
            (this._letter === 'I' && this._number >= 4 && this._number <= 11) ||
            (this._letter === 'J' && this._number >= 5 && this._number <= 11) ||
            (this._letter === 'K' && this._number >= 7 && this._number <= 10);
    }

    letter() {
        return this._letter;
    }

    number() {
        return this._number;
    }

    to_string() {
        return this._letter + this._number;
    }
}

export default Coordinates;
