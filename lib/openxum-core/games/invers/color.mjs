"use strict";

const Color = {NONE: -1, RED: 0, YELLOW: 1};

export default Color;
