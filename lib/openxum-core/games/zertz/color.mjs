"use strict";

const Color = {NONE: -1, ONE: 0, TWO: 1};

export default Color;
