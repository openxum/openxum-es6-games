"use strict";

const MoveType = {PUT_MARBLE: 0, REMOVE_RING: 1, CAPTURE: 2};

export default MoveType;
