"use strict";

import Color from "./color.mjs";
import Coordinates from "./coordinates.mjs";
import MoveType from "./move_type.mjs";

class Move {
    constructor(type, color, from = null, to = null) {
        this._type = type;
        this._color = color;
        this._from = from;
        this._to = to;
    }

    decode(str) {
        let type = str.substring(0, 1);

        if (type === 'C') {
            this._type = MoveType.CAPTURE;
        } else if (type === 'M') {
            this._type = MoveType.MOVE;
        } else if (type === 'P') {
            this._type = MoveType.PASS;
        }
        this._color = str.charAt(1) === 'B' ? Color.BLACK : Color.WHITE;
        if (this._type === MoveType.CAPTURE) {
            this._from = new Coordinates(str.charCodeAt(2) - 'A'.charCodeAt(0), parseInt(str.charAt(3)) - 1);
        } else if (this._type === MoveType.MOVE) {
            this._from = new Coordinates(str.charCodeAt(2) - 'A'.charCodeAt(0), parseInt(str.charAt(3)) - 1);
            this._to = new Coordinates(str.charCodeAt(4) - 'A'.charCodeAt(0), parseInt(str.charAt(5)) - 1);
        }
    }

    encode() {
        if (this._type === MoveType.CAPTURE) {
            return ('C' + (this._color === Color.BLACK ? "B" : "W") + this._from.to_string());
        } else if (this._type === MoveType.MOVE) {
            return ('M' + (this._color === Color.BLACK ? "B" : "W") + this._from.to_string() + this._to.to_string());
        } else if (this._type === MoveType.PASS) {
            return ('P' + (this._color === Color.BLACK ? "B" : "W"));
        }
    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        if (data.type === MoveType.MOVE) {
            this._to = new Coordinates(data.to.x, data.to.y);
            this._from = new Coordinates(data.from.x, data.from.y);
        } else if (data.type === MoveType.CAPTURE) {
            this._to = null;
            this._from = new Coordinates(data.from.x, data.from.y);
        } else if (this._type === MoveType.PASS) {
            this._to = null;
            this._from = null;
        }
    }

    to_object() {
        return {
            type: this._type,
            color: this._color,
            from: this._from === null ? {x: -1, y: -1} : {x: this._from._column, y: this._from._line},
            to: this._to === null ? {x: -1, y: -1} : {x: this._to._column, y: this._to._line},
        };
    }

    to_string() {
        if (this._type === MoveType.CAPTURE) {
            return 'capture ' + (this._color === Color.BLACK ? 'black ' : 'white ') + 'entity at ' + this._from.to_string();
        } else if (this._type === MoveType.MOVE) {
            return 'move ' + (this._color === Color.BLACK ? 'black ' : 'white ') + 'entity from ' + this._from.to_string() + ' to ' + this._to.to_string();
        } else if (this._type === MoveType.PASS) {
            return 'pass ' + (this._color === Color.BLACK ? 'black ' : 'white ');
        }
    }
}

export default Move;