import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';
import Color from "./color.mjs";
import Entity from './entity.mjs';
import Coordinates from "./coordinates.mjs";
import Move from "./move.mjs";
import MoveType from "./move_type.mjs";
import Komivoki from "./index.mjs";

class Engine extends OpenXum.Engine {
    constructor(t, c) {
        super();
        this._type = t;
        this._size = [8, 8];
        this._max_length = 3;
        this._initial_color = c;
        this._board = [];
        this._ids = [];
        for (let i = 0; i < this._size[0] * this._size[1]; i++) {
            this._board.push([]);
        }
        this._init();
        this._build_deltas();
    }

    build_move() {
        return new Move();
    }

    change_color() {
        this._current_color = this.next_color(this._current_color);
    }

    clone() {
        return new Engine(this._type, this._current_color);
        // TODO
    }

    current_color() {
        return this._current_color;
    }

    get_type() {
        return this._type;
    }

    get_name() {
        return 'komivoki';
    }

    get_possible_move_list() {
        let moves = [];

        if (this._phase === Phase.MOVE_ENTITY) {
            for (let x = 0; x < this._size[0]; x++) {
                for (let y = 0; y < this._size[1]; y++) {
                    if (this._board[x + this._size[0] * y].length > 0 && this._board[x + this._size[0] * y][0]._color === this._current_color) {
                        let from = new Coordinates(x, y);

                        this._deltas.forEach(p => {
                            let to = new Coordinates(x + p[0], y + p[1]);

                            if (this._is_possible_cell(this._current_color, from, to)) {
                                if (this._is_new_state(this._current_color, from, to)) {
                                    moves.push(new Move(MoveType.MOVE, this._current_color, from, to));
                                }
                            }
                        });
                    }
                }
            }
        } else if (this._phase === Phase.CAPTURE_ENTITY_BEFORE || this._phase === Phase.CAPTURE_ENTITY_AFTER) {
            for (let x = 0; x < this._size[0]; x++) {
                for (let y = 0; y < this._size[1]; y++) {
                    let from = new Coordinates(x, y);

                    if (this._is_possible_to_capture(this._current_color, from)) {
                        moves.push(new Move(MoveType.CAPTURE, this._current_color, from, null));
                    }
                }
            }
        }
        if (moves.length === 0) {
            moves.push(new Move(MoveType.PASS, this._current_color));
        }
        return moves;
    }

    hash(color) {
        let state = "";

        for (let x = 0; x < this._size[0]; x++) {
            for (let y = 0; y < this._size[1]; y++) {
                if (this._board[x + this._size[0] * y].length <= 0) {
                    state += "X";
                } else if (this._board[x + this._size[0] * y][0]._color === color) {
                    state += this._board[x + this._size[0] * y][0]._color === Color.BLACK ? "B" : "W";
                    this._board[x + this._size[1] * y].forEach(e => {
                        state += e._level;
                    })
                } else {
                    state += "X";
                }
            }
        }
        return state;
    }

    is_finished() {
        return this._phase === Phase.FINISH;
    }

    move(m) {
        ++this._move_number;
        if (m._type === MoveType.MOVE) {
            this._pass = 0;
            this._board[m._to._column + this._size[0] * m._to._line].push(this._board[m._from._column + this._size[0] * m._from._line].pop());
            if (this._is_possible_to_capture_color(this._current_color)) {
                this._phase = Phase.CAPTURE_ENTITY_AFTER;
            } else {
                this.change_color();
                if (this._is_possible_to_capture_color(this._current_color)) {
                    this._phase = Phase.CAPTURE_ENTITY_BEFORE;
                } else {
                    this._phase = Phase.MOVE_ENTITY;
                }
            }
        } else if (m._type === MoveType.CAPTURE) {
            const cell = this._board[m._from._column + this._size[0] * m._from._line];

            this._pass = 0;
            if (this._current_color === Color.BLACK) {
                this._white_entity_number -= cell.length;
            } else {
                this._black_entity_number -= cell.length;
            }
            this._board[m._from._column + this._size[0] * m._from._line] = [];
            if ((this._current_color === Color.BLACK && this._white_entity_number === 0) ||
                (this._current_color === Color.WHITE && this._black_entity_number === 0)) {
                this._phase = Phase.FINISH;
            } else {
                if (this._phase === Phase.CAPTURE_ENTITY_BEFORE) {
                    if (!this._is_possible_to_capture_color(this._current_color)) {
                        this._phase = Phase.MOVE_ENTITY;
                    }
                } else {
                    if (!this._is_possible_to_capture_color(this._current_color)) {
                        this.change_color();
                        if (this._is_possible_to_capture_color(this._current_color)) {
                            this._phase = Phase.CAPTURE_ENTITY_BEFORE;
                        } else {
                            this._phase = Phase.MOVE_ENTITY;
                        }
                    }
                }
            }
        } else if (m._type === MoveType.PASS) {
            ++this._pass;
            if (this._pass === 2) {
                this._phase = Phase.FINISH;
            } else {
                this.change_color();
                this._phase = Phase.MOVE_ENTITY;
            }
        }
        this._ids[this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK].push(this.hash(this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK));
    }

    next_color(c) {
        return c === Color.WHITE ? Color.BLACK : Color.WHITE;
    }

    parse(str) {
        // TODO
    }

    reset() {
        this._board.forEach((cell) => {
            while (cell.length > 0) cell.pop();
        });
        this._ids.forEach((id) => {
            while (id.length > 0) id.pop();
        });
        this._init();
    }

    to_string() {
        let string = "";

        for (let y = 0; y < this._size[1]; y++) {
            for (let x = 0; x < this._size[0]; x++) {
                string += "[";
                if (this._board[x + this._size[0] * y].length <= 0) {
                    string += "      ";
                } else {
                    let cell = this._board[x + this._size[0] * y];
                    let capacity = 0;
                    cell.forEach(c => {
                        capacity += c._level;
                    });
                    string += cell[0]._color === Color.BLACK ? "B" : "W";
                    string += capacity;
                    string += "/";
                    string += cell[cell.length - 1]._level;
                    string += "/";
                    string += cell.length;
                }
                string += "]";
            }
            string += "\n";
        }
        return string;
    }

    winner_is() {
        if (this.is_finished()) {
            if (this._pass === 2) {
                return Color.NONE;
            } else {
                return this._current_color;
            }
        } else {
            return Color.NONE;
        }
    }

// private methods
    _build_board() {
        this._build_piece(0, 0, Color.BLACK, 2);
        this._build_piece(1, 0, Color.BLACK, 3);
        this._build_piece(2, 0, Color.BLACK, 4);
        this._build_piece(3, 0, Color.BLACK, 5);
        this._build_piece(4, 0, Color.BLACK, 6);
        this._build_piece(5, 0, Color.BLACK, 4);
        this._build_piece(6, 0, Color.BLACK, 3);
        this._build_piece(7, 0, Color.BLACK, 2);

        this._build_piece(0, 1, Color.BLACK, 1);
        this._build_piece(1, 1, Color.BLACK, 1);
        this._build_piece(2, 1, Color.BLACK, 1);
        this._build_piece(3, 1, Color.BLACK, 1);
        this._build_piece(4, 1, Color.BLACK, 1);
        this._build_piece(5, 1, Color.BLACK, 1);
        this._build_piece(6, 1, Color.BLACK, 1);
        this._build_piece(7, 1, Color.BLACK, 1);

        this._build_piece(0, 7, Color.WHITE, 2);
        this._build_piece(1, 7, Color.WHITE, 3);
        this._build_piece(2, 7, Color.WHITE, 4);
        this._build_piece(3, 7, Color.WHITE, 5);
        this._build_piece(4, 7, Color.WHITE, 6);
        this._build_piece(5, 7, Color.WHITE, 4);
        this._build_piece(6, 7, Color.WHITE, 3);
        this._build_piece(7, 7, Color.WHITE, 2);

        this._build_piece(0, 6, Color.WHITE, 1);
        this._build_piece(1, 6, Color.WHITE, 1);
        this._build_piece(2, 6, Color.WHITE, 1);
        this._build_piece(3, 6, Color.WHITE, 1);
        this._build_piece(4, 6, Color.WHITE, 1);
        this._build_piece(5, 6, Color.WHITE, 1);
        this._build_piece(6, 6, Color.WHITE, 1);
        this._build_piece(7, 6, Color.WHITE, 1);
    }

    _build_deltas() {
        this._deltas = [];
        this._deltas.push([-1, -1]);
        this._deltas.push([-1, 0]);
        this._deltas.push([-1, 1]);
        this._deltas.push([0, -1]);
        this._deltas.push([0, 1]);
        this._deltas.push([1, -1]);
        this._deltas.push([1, 0]);
        this._deltas.push([1, 1]);
    }

    _build_piece(x, y, color, level) {
        this._board[x + this._size[0] * y].push(new Entity(color, level));
        if (color === Color.BLACK) {
            this._black_entity_number++;
        } else {
            this._white_entity_number++;
        }
    }

    _get_group_number(matrix, color) {
        let group_index = 1;
        let group_matrix = new Array(this._size[0] * this._size[1]).fill(0);

        for (let x = 0; x < this._size[0]; ++x) {
            for (let y = 0; y < this._size[1]; ++y) {
                if (matrix[x + y * this._size[0]] && group_matrix[x + y * this._size[0]] === 0) {
                    let stack = [];

                    stack.push([x, y]);
                    while (stack.length > 0) {
                        const coordinates = stack.pop();

                        group_matrix[coordinates[0] + coordinates[1] * this._size[0]] = group_index;
                        this._deltas.forEach((p) => {
                            const neighbour = [coordinates[0] + p[0], coordinates[1] + p[1]];

                            if (matrix[neighbour[0] + neighbour[1] * this._size[0]] &&
                                group_matrix[neighbour[0] + neighbour[1] * this._size[0]] === 0) {
                                stack.push(neighbour);
                            }
                        });
                    }
                    ++group_index;
                }
            }
        }
        --group_index;
        return group_index;
    }

    _init() {
        this._current_color = this._initial_color;
        this._phase = Phase.MOVE_ENTITY;
        this._black_entity_number = 0;
        this._white_entity_number = 0;
        this._ids.push([]);
        this._ids.push([]);
        this._pass = 0;
        this._move_number = 0;
        this._is_finished = false;
        this._build_board();
        this._ids[Color.BLACK].push(this.hash(Color.BLACK));
        this._ids[Color.WHITE].push(this.hash(Color.WHITE));
    }

    _is_new_state(color, from, to) {
        let state = "";

        for (let x = 0; x < this._size[0]; x++) {
            for (let y = 0; y < this._size[1]; y++) {
                if (from._column === x && from._line === y) {
                    if (this._board[from._column + this._size[0] * from._line].length === 1) {
                        state += "X"
                    } else {
                        state += this._board[from._column + this._size[0] * from._line][0]._color === Color.BLACK ? "B" : "W";

                        for (let i = 0; i < this._max_length - 1; i++) {
                            state += this._board[from._column + this._size[0] * from._line][i]._level;
                        }
                    }
                } else if (to._column === x && to._line === y) {
                    state += this._board[from._column + this._size[0] * from._line][0]._color === Color.BLACK ? "B" : "W";
                    this._board[to._column + this._size[0] * to._line].forEach(e => {
                        state += e._level;
                    });
                    state += this._board[from._column + this._size[0] * from._line][0]._level;
                } else {
                    if (this._board[x + this._size[0] * y].length <= 0) {
                        state += "X";
                    } else if (this._board[x + this._size[0] * y].length > 0 && this._board[x + this._size[0] * y][0]._color === this.current_color()) {
                        state += this._board[x + this._size[0] * y][0]._color === Color.BLACK ? "B" : "W";
                        this._board[x + this._size[0] * y].forEach(e => {
                            state += e._level;
                        });
                    } else {
                        state += "X";
                    }
                }
            }
        }
        return !this._ids[this._current_color].includes(state);
    }

    _is_possible_cell(color, from, to) {
        if (to.in_range(this._size)) {
            const cell = this._board[to._column + this._size[0] * to._line];
            const ok = cell.length === 0 || (cell.length > 0 && cell[0]._color === color && cell.length < this._max_length);

            if (this._type === Komivoki.GameType.VARIANT_1) {
                return ok;
            } else if (this._type === Komivoki.GameType.VARIANT_2) {
                const origin_matrix = [];

                this._board.forEach((cell) => {
                    if (cell.length > 0 && cell[0]._color === color) {
                        origin_matrix.push(true);
                    } else {
                        origin_matrix.push(false);
                    }
                });

                const modified_matrix = [];

                for (let x = 0; x < this._size[0]; ++x) {
                    for (let y = 0; y < this._size[1]; ++y) {
                        const coordinates = [x, y];

                        if (coordinates[0] === to.column() && coordinates[1] === to.line()) {
                            modified_matrix.push(true);
                        } else {
                            const cell = this._board[x + y * this._size[0]];

                            if (coordinates[0] === from.column() && coordinates[1] === from.line()) {
                                if (cell.length === 1) {
                                    modified_matrix.push(false);
                                } else {
                                    modified_matrix.push(true);
                                }
                            } else {
                                if (cell.length > 0 && cell[0]._color === color) {
                                    modified_matrix.push(true);
                                } else {
                                    modified_matrix.push(false);
                                }
                            }
                        }
                    }
                }
                if (this._get_group_number(modified_matrix, color) <= this._get_group_number(origin_matrix, color)) {
                    return ok;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    _is_possible_to_capture_color(color) {
        for (let x = 0; x < this._size[0]; x++) {
            for (let y = 0; y < this._size[1]; y++) {
                if (this._is_possible_to_capture(color, new Coordinates(x, y))) {
                    return true;
                }
            }
        }
        return false;
    }

    _is_possible_to_capture(color, coordinates) {
        if (this._board[coordinates._column + this._size[0] * coordinates._line].length !== 0) {
            const cell = this._board[coordinates._column + this._size[0] * coordinates._line];

            if (cell.length !== 0 && cell[0]._color !== color) {
                let sum = 0;
                let neighbor_sum = 0;
                let level_sum = 0;
                let capacity = 0;

                cell.forEach(e => {
                    capacity += e._level;
                })
                this._deltas.forEach(p => {
                    let c = new Coordinates(coordinates._column + p[0], coordinates._line + p[1]);

                    if (c.in_range(this._size)) {
                        let neighbor = this._board[c._column + this._size[0] * c._line];

                        if (neighbor.length > 0 && neighbor[0]._color === color) {
                            neighbor_sum += neighbor.length;
                            sum += neighbor.length;
                            neighbor.forEach(e => {
                                level_sum += e._level;
                            });
                        }
                    } else {
                        ++sum;
                    }
                });
                if (neighbor_sum > 0 && ((sum >= cell.length && level_sum > capacity) || (sum > cell.length && level_sum >= capacity))) {
                    return true;
                }
            }
        }
        return false;
    }
}

export default Engine;