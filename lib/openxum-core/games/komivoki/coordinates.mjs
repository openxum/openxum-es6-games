"use strict";

class Coordinates {
    constructor(c = -1, l = -1) {
        this._column = c;
        this._line = l;
    }

    clone() {
        return new Coordinates(this._column, this._line);
    }

    column() {
        return this._column;
    }

    equals(coordinates) {
        return this._column === coordinates.get_column() &&
            this._line === coordinates.get_line();
    }

    in_range(size) {
        return this._column >= 0 && this._column < size[0] && this._line >= 0 && this._line < size[1];
    }

    is_valid() {
        return this._column !== -1 && this._column !== -1;
    }

    line() {
        return this._line;
    }

    to_string() {
        return String.fromCharCode('A'.charCodeAt(0) + this._column) + (this._line + 1);
    }
}

export default Coordinates;