"use strict";

// namespace Komivoki
import GameType from './game_type.mjs';
import Color from './color.mjs';
import Coordinates from "./coordinates.mjs";
import Engine from './engine.mjs';
import Entity from './entity.mjs';
import Move from './move.mjs';
import MoveType from "./move_type.mjs";
import Phase from './phase.mjs';

export default {
    Color: Color,
    Coordinates: Coordinates,
    Engine: Engine,
    Entity: Entity,
    GameType: GameType,
    Move: Move,
    MoveType: MoveType,
    Phase: Phase
};
