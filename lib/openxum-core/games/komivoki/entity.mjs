"use strict";

class Entity {
    constructor(c, l) {
        this._color = c;
        this._level = l;
    }
}

export default Entity;