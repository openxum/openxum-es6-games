"use strict";

const Phase = {MOVE_ENTITY: 0, CAPTURE_ENTITY_AFTER: 1, CAPTURE_ENTITY_BEFORE: 2, FINISH: 3};

export default Phase;