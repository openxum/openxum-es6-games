"use strict";

const Color = {
    NONE: -1,
    BLACK: 0,
    WHITE: 1,
    to_string(c) {
        if (c === Color.NONE) {
            return 'none';
        }
        if (c === Color.BLACK) {
            return 'black';
        }
        if (c === Color.WHITE) {
            return 'white';
        }
    }
};

export default Color;
