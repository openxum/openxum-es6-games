"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import OpenXum from '../../openxum/index.mjs';
import MoveType from './move_type.mjs';

class Move extends OpenXum.Move {
    constructor(t, c, to, i) {
        super();
        this._type = t;
        this._color = c;
        this._to = to;
        this._index = i;
    }

// public methods
    color() {
        return this._color;
    }

    decode(str) {
        this._color = str[0] === 'B' ? Color.BLACK : Color.WHITE;
        if (str[1] === '-') {
            this._type = MoveType.PASS;
            this._to = null;
            this._index = -1;
        } else if (str[1] === "P") {
            this._type = MoveType.CHOICE_PATTERN;
            this._to = null;
            this._index = parseInt(str[2]);
        } else if (str[1] === "p") {
            this._type = MoveType.CHOICE_PIECE;
            this._to = null;
            this._index = parseInt(str[2]);
        } else {
            this._type = str[1] === "+" ? MoveType.PUT_PIECE : MoveType.PUT_SHIDO;
            if (str.length === 4) {
                this._to = new Coordinates(str.charCodeAt(2) - 'A'.charCodeAt(0),
                    str.charCodeAt(3) - '1'.charCodeAt(0));
            } else {
                this._to = new Coordinates(str.charCodeAt(2) - 'A'.charCodeAt(0),
                    (str.charCodeAt(3) - '0'.charCodeAt(0)) * 10 + (str.charCodeAt(4) - '0'.charCodeAt(0)) - 1);
            }
            this._index = -1;
        }
    }

    encode() {
        if (this._type === MoveType.PASS) {
            return (this._color === Color.BLACK ? 'B' : 'W') + '-';
        } else if (this._type === MoveType.CHOICE_PATTERN) {
            return (this._color === Color.BLACK ? 'B' : 'W') + "P" + this._index;
        } else if (this._type === MoveType.CHOICE_PIECE) {
            return (this._color === Color.BLACK ? 'B' : 'W') + "p" + this._index;
        } else if (this._type === MoveType.PUT_PIECE) {
            return (this._color === Color.BLACK ? 'B' : 'W') + "+" + this._to.to_string();
        } else if (this._type === MoveType.PUT_SHIDO) {
            return (this._color === Color.BLACK ? 'B' : 'W') + "*" + this._to.to_string();
        }
    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        this._to = new Coordinates(data.to.column, data.to.line);
        this._index = data.index;
    }

    index() {
        return this._index;
    }

    to() {
        return this._to;
    }

    to_object() {
        return {
            type: this._type,
            color: this._color,
            to: this._to === null ? {column: -1, line: -1} : {column: this._to._column, line: this._to._line},
            index: this._index
        };
    }

    to_string() {
        if (this._type === MoveType.PASS) {
            return 'PASS';
        } else if (this._type === MoveType.CHOICE_PATTERN) {
            return 'CHOICE pattern ' + this._index;
        } else if (this._type === MoveType.CHOICE_PIECE) {
            if (this._index === 0) {
                return 'CHOICE shido';
            } else {
                return 'CHOICE piece';
            }
        } else if (this._type === MoveType.PUT_PIECE) {
            return 'PUT ' + (this._color === Color.BLACK ? 'black' : 'white') + ' PIECE at ' +
                this._to.to_string();
        } else if (this._type === MoveType.PUT_SHIDO) {
            return 'PUT ' + (this._color === Color.BLACK ? 'black' : 'white') + ' SHIDO at ' +
                this._to.to_string();
        }
    }

    type() {
        return this._type;
    }
}

export default Move;
