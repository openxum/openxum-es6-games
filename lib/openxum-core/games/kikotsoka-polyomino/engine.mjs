"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Patterns from './patterns.mjs';
import Polyomino from './polyomino.mjs';
import Polyominos from './polyominos.mjs';
import Phase from './phase.mjs';
import State from './state.mjs';

const config = [
    {
        size: 20,
        polyomino_number: 24
    }
];

const black_points = [
    [[1, 1, 1], [1, 0, 1], [1, 1, 1]],
    [[0, 0, 1], [0, 0, 0], [0, 0, 1]],
    [[1, 1, 1], [0, 1, 0], [0, 0, 0]],
    [[1, 1, 1], [1, 0, 0], [1, 1, 1]],
    [[0, 0, 0], [1, 0, 0], [1, 1, 1]],
    [[1, 0, 0], [1, 1, 0], [1, 1, 1]],
    [[0, 1, 0], [1, 0, 0], [0, 1, 1]],
    [[0, 0, 1], [1, 1, 0], [0, 1, 0]],
    [[1, 0, 1], [1, 0, 0], [0, 1, 1]],
    [[0, 1, 1], [1, 1, 1], [0, 0, 0]],
    [[0, 0, 0], [0, 1, 0], [1, 1, 1]],
    [[0, 0, 0], [1, 1, 0], [0, 1, 0]],
    [[1, 1, 1], [1, 1, 1], [0, 1, 0]],
    [[0, 0, 1], [1, 1, 1], [0, 0, 1]],
    [[0, 1, 0], [0, 0, 0], [1, 1, 1]],
    [[1, 1, 1], [0, 1, 1], [1, 0, 1]],
    [[1, 0, 0], [1, 0, 0], [1, 0, 1]],
    [[1, 0, 0], [0, 0, 1], [0, 1, 1]],
    [[1, 1, 1], [0, 0, 1], [0, 1, 1]],
    [[1, 1, 0], [1, 0, 1], [1, 1, 1]],
    [[1, 1, 0], [0, 0, 0], [1, 1, 0]],
    [[1, 1, 0], [0, 1, 0], [0, 1, 1]],
    [[0, 1, 1], [1, 0, 0], [1, 0, 0]],
    [[0, 1, 1], [1, 0, 0], [0, 1, 1]]];

const black_indexes = [7, 4, 9, 1, 2, 8, 9, 1, 5, 4, 11, 10, 6, 13, 3, 1, 9, 9, 12, 4, 13, 0, 1, 9];

const white_points = [
    [[0, 1, 0], [0, 0, 0], [0, 0, 1]],
    [[0, 1, 0], [1, 1, 0], [0, 0, 1]],
    [[1, 0, 1], [0, 1, 1], [0, 0, 0]],
    [[0, 0, 0], [1, 1, 1], [1, 1, 0]],
    [[0, 0, 1], [0, 1, 1], [0, 1, 0]],
    [[0, 0, 0], [0, 1, 0], [0, 1, 1]],
    [[0, 1, 1], [1, 0, 0], [1, 1, 1]],
    [[0, 0, 1], [0, 1, 0], [0, 1, 0]],
    [[1, 1, 0], [0, 1, 0], [0, 0, 1]],
    [[1, 1, 0], [0, 0, 1], [0, 1, 1]],
    [[1, 1, 0], [0, 1, 1], [0, 1, 1]],
    [[1, 0, 1], [1, 1, 1], [0, 0, 0]],
    [[0, 1, 0], [1, 0, 1], [1, 0, 0]],
    [[1, 1, 0], [0, 1, 0], [1, 0, 1]],
    [[0, 1, 1], [0, 0, 1], [0, 1, 0]],
    [[1, 0, 1], [1, 0, 1], [1, 0, 1]],
    [[0, 0, 1], [1, 1, 1], [0, 0, 0]],
    [[0, 1, 1], [0, 1, 1], [1, 1, 0]],
    [[0, 0, 1], [1, 1, 0], [0, 0, 0]],
    [[1, 1, 0], [0, 1, 0], [0, 1, 1]],
    [[1, 0, 1], [0, 0, 1], [1, 0, 1]],
    [[1, 0, 0], [1, 0, 1], [1, 0, 1]],
    [[1, 0, 0], [1, 1, 1], [1, 1, 0]],
    [[0, 1, 1], [0, 0, 1], [1, 0, 1]]];

const white_indexes = [3, 9, 5, 10, 7, 6, 7, 0, 2, 12, 11, 4, 8, 9, 1, 8, 9, 7, 5, 13, 9, 8, 9, 6];

class Engine extends OpenXum.Engine {
    constructor(t, c) {
        super();
        this._type = t;
        this._color = c;
        this._board = [];
        for (let l = 0; l < config[t].size; ++l) {
            this._board[l] = [];
            for (let c = 0; c < config[t].size; ++c) {
                this._board[l][c] = State.VACANT;
            }
        }
        this._black_polyominos = [];
        this._white_polyominos = [];
        this._init_polyominos(Color.BLACK, this._black_polyominos, config[t].polyomino_number);
        this._init_polyominos(Color.WHITE, this._white_polyominos, config[t].polyomino_number);
        this._black_polyomino_number = this._black_polyominos.length;
        this._white_polyomino_number = this._white_polyominos.length;
        this._last_coordinates = null;
        this._pass = 0;
        this._black_captured_piece_number = 0;
        this._white_captured_piece_number = 0;
        this._black_level = 0;
        this._white_level = 0;
        this._black_failed = false;
        this._white_failed = false;
        this._size = config[t].size;
        this._phase = Phase.PUT_POLYOMINO;
    }

// public methods
    build_move() {
        return new Move();
    }

    clone() {
        let o = new Engine(this._type, this._color);

        for (let l = 0; l < this._size; ++l) {
            for (let c = 0; c < this._size; ++c) {
                o._board[l][c] = this._board[l][c];
            }
        }
        o._black_polyominos = [];
        o._white_polyominos = [];
        for (let k = 0; k < this._black_polyominos.length; ++k) {
            o._black_polyominos.push(this._black_polyominos[k].clone());
        }
        for (let k = 0; k < this._white_polyominos.length; ++k) {
            o._white_polyominos.push(this._white_polyominos[k].clone());
        }
        o._black_polyomino_number = this._black_polyomino_number;
        o._white_polyomino_number = this._white_polyomino_number;
        o._last_coordinates = this._last_coordinates;
        o._pass = this._pass;
        o._black_captured_piece_number = this._black_captured_piece_number;
        o._white_captured_piece_number = this._white_captured_piece_number;
        o._black_level = this._black_level;
        o._white_level = this._white_level;
        o._black_failed = this._black_failed;
        o._white_failed = this._white_failed;
        o._size = this._size;
        o._phase = this._phase;
        return o;
    }

    current_color() {
        return this._color;
    }

    get_name() {
        return 'kikotsoka-polyomino';
    }

    get_possible_move_list() {
        let list = [];

        if (this._phase === Phase.PUT_POLYOMINO) {
            const first = this._color === Color.BLACK ? this._black_polyomino_number === this._black_polyominos.length :
                this._white_polyomino_number === this._white_polyominos.length;
            const polyominos = this._color === Color.BLACK ? this._black_polyominos : this._white_polyominos;

            for (let k = 0; k < polyominos.length; ++k) {
                if (polyominos[k].is_free()) {
                    let polyomino = polyominos[k].clone();

                    for (let r = 0; r < Polyominos[polyomino.shape_index()].length; ++r) {
                        for (let l = 0; l < this._size; ++l) {
                            for (let c = 0; c < this._size; ++c) {
                                if (this.is_valid(polyomino, {line: l, column: c}, first)) {
                                    list.push(new Move(MoveType.PUT_POLYOMINO, this._color, new Coordinates(c, l), [k, r], -1));
                                }
                            }
                        }
                        polyomino.rotate();
                    }
                }
            }
            if (list.length === 0) {
                list.push(new Move(MoveType.PASS, this._color, null, null, -1));
            }
        } else { // phase = Phase.CHOICE_PATTERN
            const result = this._check_patterns();

            for (let i = 0; i < result.length; ++i) {
                list.push(new Move(MoveType.CHOICE_PATTERN, this._color, null, null, i));
            }
        }
        return list;
    }

    get_type() {
        return this._type;
    }

    is_finished() {
        return this._black_level === 5 || this._white_level === 5 || this._pass === 2 || this._black_failed || this._white_failed ||
            (this._black_polyomino_number === 0 && this._white_polyomino_number === 0);
    }

    is_valid(polyomino, coordinates, first) {
        const shape = polyomino.shape();
        let ok = true;
        let i = -1;
        let counter = 0;

        while (ok && i < 2) {
            const line = coordinates.line + i;
            let j = -1;

            while (ok && j < 2) {
                const column = coordinates.column + j;

                ok = (line >= 0 && column >= 0 && line < this._size && column < this._size) ||
                    (column < 0 && shape[i + 1][j + 1] === 0) || (line < 0 && shape[i + 1][j + 1] === 0) ||
                    (column >= this._size && shape[i + 1][j + 1] === 0) || (line >= this._size && shape[i + 1][j + 1] === 0);
                if (ok) {
                    if (line >= 0 && column >= 0 && line < this._size && column < this._size) {
                        ok = this._board[line][column] === State.VACANT || shape[i + 1][j + 1] === 0;
                    }
                }
                if (ok && !first) {
                    if (shape[i + 1][j + 1] !== 0) {
                        for (let p = -1; p < 2; ++p) {
                            for (let r = -1; r < 2; ++r) {
                                if (this._is_neighbour(line + p, column + r)) {
                                    ++counter;
                                }
                            }
                        }
                    }
                }
                ++j
            }
            ++i;
        }
        ok = ok && (first || (!first && counter > 0));
        if (ok && !first) {
            let e = this.clone();

            e._put_polyomino(polyomino.clone(), new Coordinates(coordinates.column, coordinates.line));
            e._check_patterns();
            ok = !e._black_failed && !e._white_failed;
        }
        return ok;
    }

    move(move) {
        if (move !== null) {
            if (move.type() === MoveType.PUT_POLYOMINO) {
                let polyomino = move.color() === Color.BLACK ? this._black_polyominos[move.polyomino_index()] :
                    this._white_polyominos[move.polyomino_index()];

                for (let i = polyomino.rotation(); i < move.rotation(); ++i) {
                    polyomino.rotate();
                }
                this._put_polyomino(polyomino, move.to());

                const result = this._check_patterns();

                if (result.length > 0) {
                    if (result.length === 1) {
                        this._capture(result[0]);
                        this._block(result[0]);
                        if (this._color === Color.BLACK) {
                            ++this._black_level;
                        } else {
                            ++this._white_level;
                        }
                        this._change_color();
                        this._phase = Phase.PUT_POLYOMINO;
                    } else {
                        this._phase = Phase.CHOICE_PATTERN;
                    }
                } else {
                    this._change_color();
                    this._phase = Phase.PUT_POLYOMINO;
                }
                this._pass = 0;
            } else if (move.type() === MoveType.CHOICE_PATTERN) {
                const result = this._check_patterns();

                this._capture(result[move.index()]);
                this._block(result[move.index()]);
                if (this._color === Color.BLACK) {
                    ++this._black_level;
                } else {
                    ++this._white_level;
                }
                this._change_color();
                this._phase = Phase.PUT_POLYOMINO;
            } else if (move.type() === MoveType.PASS) {
                ++this._pass;
                this._last_coordinates = null;
                this._change_color();
                this._phase = Phase.PUT_POLYOMINO;
            }
        }
    }

    parse(str) {
        // TODO
    }

    phase() {
        return this._phase;
    }

    to_string() {
        let str = '   ';

        for (let c = 0; c < this._size; ++c) {
            str += ' ' + String.fromCharCode('A'.charCodeAt(0) + c) + ' ';
        }
        str += '\n';
        for (let l = 0; l < this._size; ++l) {
            if (l < 9) {
                str += (l + 1) + '  ';
            } else {
                str += (l + 1) + ' ';
            }
            for (let c = 0; c < this._size; ++c) {
                str += State.to_string(this._board[l][c]);
            }
            str += '\n';
        }
        return str;
    }

    winner_is() {
        if (this.is_finished()) {
            if (this._black_level === 5 || this._black_level > this._white_level || this._white_failed) {
                return Color.BLACK;
            } else if (this._white_level === 5 || this._black_level < this._white_level || this._black_failed) {
                return Color.WHITE;
            } else {
                if (this._black_captured_piece_number > this._white_captured_piece_number) {
                    return Color.BLACK;
                } else if (this._black_captured_piece_number < this._white_captured_piece_number) {
                    return Color.WHITE;
                } else {
                    return Color.NONE;
                }
            }
        } else {
            return false;
        }
    }

// private methods
    _block(origin) {
        let l = origin.l;
        let c = origin.c;

        while (l < origin.l + 3) {
            this._block_coordinates(l, c);
            ++c;
            if (c === origin.c + 3) {
                c = origin.c;
                ++l;
            }
        }
    }

    _block_coordinates(l, c) {
        const old_value = this._board[l][c];
        let new_value = this._color === Color.BLACK ? State.BLOCKED_IN_BLACK : State.BLOCKED_IN_WHITE;

        if (old_value === State.BLACK && this._color === Color.BLACK) {
            new_value = State.BLACK_BLOCKED;
        } else if (old_value === State.WHITE && this._color === Color.WHITE) {
            new_value = State.WHITE_BLOCKED;
        }
        this._board[l][c] = new_value;
    }

    _capture(origin) {
        let l = origin.l;
        let c = origin.c;
        let n = 0;

        while (l < origin.l + 3) {
            if (this._board[l][c] === State.BLACK && this._color === Color.WHITE) {
                this._board[l][c] = State.VACANT;
                ++n;
            }
            if (this._board[l][c] === State.WHITE && this._color === Color.BLACK) {
                this._board[l][c] = State.VACANT;
                ++n;
            }
            ++c;
            if (c === origin.c + 3) {
                c = origin.c;
                ++l;
            }
        }
        if (this._color === Color.BLACK) {
            this._black_captured_piece_number += n;
        } else {
            this._white_captured_piece_number += n;
        }
    }

    _change_color() {
        this._color = this._color === Color.BLACK ? Color.WHITE : Color.BLACK;
    }

    _check_no_blocked(origin) {
        let l = origin.l;
        let c = origin.c;
        let blocked = false;

        while (!blocked && l < origin.l + 3) {
            blocked = (this._board[l][c] === State.BLOCKED_IN_BLACK || this._board[l][c] === State.BLOCKED_IN_WHITE ||
                this._board[l][c] === State.BLACK_BLOCKED || this._board[l][c] === State.WHITE_BLOCKED);
            ++c;
            if (c === origin.c + 3) {
                c = origin.c;
                ++l;
            }
        }
        return !blocked;
    }

    _check_pattern(pattern) {
        let l = 0;
        let c = 0;
        let origins = [];

        while (l < this._size - 2) {
            if (this._check_no_blocked({l: l, c: c})) {
                if (this._check_pattern_in_block({l: l, c: c}, pattern)) {
                    origins.push({l: l, c: c});
                }
            }
            ++c;
            if (c === this._size - 2) {
                c = 0;
                ++l;
            }
        }
        return origins;
    }

    _check_pattern_in_block(origin, pattern) {
        let l = origin.l;
        let c = origin.c;
        let ok = true;

        while (ok && l < origin.l + 3) {
            const value = pattern[l - origin.l][c - origin.c];

            if ((value === 0 &&
                    this._board[l][c] !== (this._color === Color.BLACK ? State.BLACK : State.WHITE)) ||
                (value === 1 &&
                    (this._board[l][c] === (this._color === Color.BLACK ? State.BLACK : State.WHITE)))) {
                ++c;
                if (c === origin.c + 3) {
                    c = origin.c;
                    ++l;
                }
            } else {
                ok = false;
            }
        }
        return ok;
    }

    _check_patterns() {
        let level = 0;
        let found = false;
        let origins = [];

        while (!found && level < 5) {
            const pattern = Patterns[level];

            for (let j = 0; j < pattern.length; ++j) {
                const new_origins = this._check_pattern(pattern[j]);

                if (new_origins.length > 0) {
                    origins = origins.concat(new_origins);
                    found = true;
                }
            }
            if (!found) {
                ++level;
            }
        }
        if (found) {
            const current_level = this._color === Color.BLACK ? this._black_level : this._white_level;

            if (level === current_level) {
                return origins;
            } else {
                if (this._color === Color.BLACK) {
                    this._black_failed = true;
                } else {
                    this._white_failed = true;
                }
                return [];
            }
        } else {
            return [];
        }
    }

    _init_polyominos(color, polyominos, size) {
        if (color === Color.BLACK) {
            for (let k = 0; k < size; ++k) {
                polyominos.push(new Polyomino(black_indexes[k], black_points[k]));
            }
        } else {
            for (let k = 0; k < size; ++k) {
                polyominos.push(new Polyomino(white_indexes[k], white_points[k]));
            }
        }

        // let str = '';
        // let str2 = '[';
        //
        // for (let k = 0; k < size; ++k) {
        //   const index = Math.floor(Math.random() * Polyominos.length);
        //   let points = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        //
        //   for (let i = 0; i < 3; ++i) {
        //     for (let j = 0; j < 3; ++j) {
        //       points[i][j] = Math.random() > 0.5 ? 1 : 0;
        //     }
        //   }
        //   polyominos.push(new Polyomino(index, points));
        //
        //   str2 += index + ',';
        //
        //   str += '[';
        //   for (let i = 0; i < 3; ++i) {
        //     str += '[';
        //     for (let j = 0; j < 3; ++j) {
        //       str += points[i][j];
        //       if (j !== 2) {
        //         str += ', ';
        //       }
        //     }
        //     str += '],';
        //   }
        //   str += '],\n';
        // }
        // str2 += ']\n';
        // console.log(str);
        // console.log(str2);
    }

    _is_neighbour(i, j) {
        return i >= 0 && j >= 0 && i < this._size && j < this._size &&
            (this._board[i][j] === (this._color === Color.BLACK ? State.BLACK_NO_VACANT : State.WHITE_NO_VACANT) ||
                this._board[i][j] === (this._color === Color.BLACK ? State.BLACK_BLOCKED : State.WHITE_BLOCKED) ||
                this._board[i][j] === (this._color === Color.BLACK ? State.BLACK : State.WHITE));
    }

    _put_polyomino(polyomino, coordinates) {
        const shape = polyomino.shape();

        polyomino.put(coordinates);
        if (this._color === Color.BLACK) {
            --this._black_polyomino_number;
        } else {
            --this._white_polyomino_number;
        }
        for (let i = -1; i < 2; ++i) {
            for (let j = -1; j < 2; ++j) {
                if (shape[i + 1][j + 1] === 2) {
                    this._board[coordinates.line() + i][coordinates.column() + j] =
                        this._color === Color.BLACK ? State.BLACK : State.WHITE;
                } else if (shape[i + 1][j + 1] === 1) {
                    this._board[coordinates.line() + i][coordinates.column() + j] = this._color === Color.BLACK ? State.BLACK_NO_VACANT : State.WHITE_NO_VACANT;
                }
            }
        }

    }
}

export default Engine;
