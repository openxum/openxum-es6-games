"use strict";

const State = {
    VACANT: 0,
    BLACK: 1,
    WHITE: 2,
    BLACK_BLOCKED: 3,
    WHITE_BLOCKED: 4,
    BLOCKED_IN_BLACK: 5,
    BLOCKED_IN_WHITE: 6,
    BLACK_NO_VACANT: 7,
    WHITE_NO_VACANT: 8,
    to_string(s) {
        if (s === State.VACANT) {
            return '[ ]';
        }
        if (s === State.BLACK) {
            return '[B]';
        }
        if (s === State.WHITE) {
            return '[W]';
        }
        if (s === State.BLACK_BLOCKED) {
            return ' b ';
        }
        if (s === State.WHITE_BLOCKED) {
            return ' w ';
        }
        if (s === State.BLOCKED_IN_BLACK || s === State.BLOCKED_IN_WHITE) {
            return '   ';
        }
        if (s === State.BLACK_NO_VACANT) {
            return '*B*';
        }
        if (s === State.WHITE_NO_VACANT) {
            return '*W*';
        }
    }
};

export default State;
