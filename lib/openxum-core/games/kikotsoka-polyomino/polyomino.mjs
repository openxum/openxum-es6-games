"use strict";

import Polyominos from './polyominos.mjs';

class Polyomino {
    constructor(s, p) {
        this._shape_index = s;
        this._rotation = 0;
        this._points = p;
        this._coordinates = null;
        this._shape = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        this._build();
    }

    clone() {
        let o = new Polyomino(this._shape_index, this._points);

        o._rotation = this._rotation;
        o._coordinates = this._coordinates;
        o._shape = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        for (let i = 0; i < 3; ++i) {
            for (let j = 0; j < 3; ++j) {
                o._shape[i][j] = this._shape[i][j];
            }
        }
        return o;
    }

    coordinates() {
        return this._coordinates;
    }

    is_free() {
        return this._coordinates === null;
    }

    put(coordinates) {
        this._coordinates = coordinates;
    }

    rotate() {
        ++this._rotation;
        this._rotation = this._rotation % 4;
        for (let i = 0; i < 1; i++) {
            for (let j = i; j < 3 - i - 1; j++) {
                const temp = this._shape[i][j];

                this._shape[i][j] = this._shape[3 - 1 - j][i];
                this._shape[3 - 1 - j][i] = this._shape[3 - 1 - i][3 - 1 - j];
                this._shape[3 - 1 - i][3 - 1 - j] = this._shape[j][3 - 1 - i];
                this._shape[j][3 - 1 - i] = temp;
            }
        }
    }

    rotation() {
        return this._rotation;
    }

    shape() {
        return this._shape;
    }

    shape_index() {
        return this._shape_index;
    }

    to_string() {
        let str = '';

        for (let i = 0; i < 3; ++i) {
            for (let j = 0; j < 3; ++j) {
                str += this._shape[i][j] === 0 ? ' ' : this._shape[i][j] === 1 ? 'o' : 'O';
            }
            str += '\n';
        }
        return str;
    }

    _build() {
        const p = Polyominos[this._shape_index][0];

        for (let i = 0; i < 3; ++i) {
            for (let j = 0; j < 3; ++j) {
                this._shape[i][j] = p[i][j];
                if (this._shape[i][j] === 1) {
                    this._shape[i][j] += this._points[i][j];
                }
            }
        }
    }
}

export default Polyomino;
