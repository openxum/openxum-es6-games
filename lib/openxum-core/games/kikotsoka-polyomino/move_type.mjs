"use strict";

const MoveType = {PUT_POLYOMINO: 0, CHOICE_PATTERN: 1, PASS: 2};

export default MoveType;