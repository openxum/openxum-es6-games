"use strict";

import Color from './color.mjs';
import Piece from './piece.mjs';
import Stack from './stack.mjs';
import State from './state.mjs';

class Intersection {
    constructor(c, cl, t) {
        this._coordinates = c;
        this._state = State.NO_VACANT;
        this._stack = new Stack();
        this._stack.put_piece(new Piece(cl, t));
    }

// public methods
    capture(destination) {
        let stack = new Stack();

        while (!this._stack.empty()) {
            stack.put_piece(this._stack.remove_top());
        }
        this._state = State.VACANT;
        destination.remove_stack();
        while (!stack.empty()) {
            const piece = stack.remove_top();

            destination.put_piece(piece.color(), piece.type());
        }
    }

    clone() {
        let intersection = new Intersection(this._coordinates.clone());

        intersection.set(this._state, this._stack.clone());
        return intersection;
    }

    color() {
        if (this._state === State.VACANT) {
            return -1;
        }
        return this._stack.color();
    }

    coordinates() {
        return this._coordinates;
    }

    hash() {
        return this._coordinates.hash();
    }

    letter() {
        return this._coordinates.letter();
    }

    move_stack_to(destination) {
        let stack = new Stack();

        while (!this._stack.empty()) {
            stack.put_piece(this._stack.remove_top());
        }
        this._state = State.VACANT;
        while (!stack.empty()) {
            const piece = stack.remove_top();

            destination.put_piece(piece.color(), piece.type());
        }
    }

    number() {
        return this._coordinates.number();
    }

    put_piece(color, type) {
        this._state = State.NO_VACANT;
        this._stack.put_piece(new Piece(color, type));
    }

    remove_stack() {
        this._state = State.VACANT;
        this._stack.clear();
    }

    set(state, stack) {
        this._state = state;
        this._stack = stack;
    }

    size() {
        return this._stack.size();
    }

    state() {
        return this._state;
    }

    type() {
        return this._stack.type();
    }
}

export default Intersection;
