import Abande from './abande/index.mjs';
import Dakapo from './dakapo/index.mjs';
import Dvonn from './dvonn/index.mjs';
import Hnefatafl from "./hnefatafl/index.mjs";
import Invers from './invers/index.mjs';
import Kikotsoka from './kikotsoka/index.mjs';
import KikotsokaPolyomino from './kikotsoka-polyomino/index.mjs';
import Mixtour from "../../openxum-core/games/mixtour/index.mjs";
import Neutreeko from "./neutreeko/index.mjs";
import Tintas from "./tintas/index.mjs";

export default {
    Abande: Abande,
    Dakapo: Dakapo,
    Dvonn: Dvonn,
    Hnefatafl: Hnefatafl,
    Invers: Invers,
    Kikotsoka: Kikotsoka,
    KikotsokaPolyomino: KikotsokaPolyomino,
    Mixtour: Mixtour,
    Neutreeko: Neutreeko,
    Tintas: Tintas
};
