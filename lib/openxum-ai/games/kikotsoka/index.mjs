"use strict";

import MCTSPlayer from './mcts_player.mjs';
import RandomPlayer from './random_player.mjs';

export default {
    MCTSPlayer: MCTSPlayer,
    RandomPlayer: RandomPlayer
};
