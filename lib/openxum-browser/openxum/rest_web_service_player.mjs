"use strict";

import OpenXum from '../../openxum-core/openxum/index.mjs';
import StatusValues from "./status_values.mjs";

async function waitUntil(condition, time = 100) {
    while (!condition()) {
        await new Promise((resolve) => setTimeout(resolve, time));
    }
}

class RestWebServicePlayer extends OpenXum.Player {
    constructor(c, o, e, t, a, p) {
        super(c, o, e);
        this._player_type = t;
        this._address = a;
        this._port = p;
        this._connected = false;
        this._started = false;
        this._apply_move = 0;
        this._next_move = null;
        this._socket = new WebSocket("ws://" + this._address + ":" + this._port + "/openxum");
        this._socket.addEventListener("message", (event) => {
            const message = JSON.parse(event.data);

            if (message.action === "connected") {
                this._connected = true;
            } else if (message.action === "started") {
                this._started = true;
            } else if (message.action === "next_move") {
                this._next_move = message.move;
            } else if (message.action === "apply_move") {
                this._apply_move = true;
            }
        });
//        this._id = -1;
    }

    // public methods
    confirm() {
        return true;
    }

    is_ready() {
        return this._started;
    }

    is_remote() {
        return true;
    }

    move(move) {
        if (move) {
            return new Promise((resolve) => {
                this._move(move).then(() => {
                    resolve(null);
                })
            });
        } else {
            return new Promise((resolve) => {
                this._move().then((data) => {
                    if (Array.isArray(data)) {
                        const moves = [];

                        data.forEach((m) => {
                            const new_move = this._manager.build_move();

                            new_move.from_object(m);
                            moves.push(new_move);
                        });
                        resolve(moves);
                    } else {
                        const new_move = this._manager.build_move();

                        new_move.from_object(data);
                        resolve(new_move);
                    }
                });
            });
        }
    }

    reinit(e) {
        this._engine = e;
    }

    async start() {
        return new Promise((resolve) => {
            this._start().then(() => {
                this._manager.ready(StatusValues.StatusValues.READY);
                if (this._engine.current_color() === this._color) {
                    this._manager.play_other(true);
                }
            });
        });
    }

    async _move(move) {
        if (move) {
            this._apply_move = false;
            if (Array.isArray(move)) {
                const moves = [];

                move.forEach((m) => {
                    moves.push(m.to_object());
                });
                this._socket.send(JSON.stringify({
                    action: "apply_move",
                    move: JSON.stringify(moves)
                }));
            } else {
                this._socket.send(JSON.stringify({
                    action: "apply_move",
                    move: JSON.stringify(move.to_object())
                }));
            }
            await waitUntil(() => this._apply_move);
            return move;
        } else {
            this._next_move = null;
            this._socket.send(JSON.stringify({
                action: "next_move"
            }));
            await waitUntil(() => this._next_move !== null);
            return this._next_move;
        }
    }

    async _start() {
        await waitUntil(() => this._connected);
        this._socket.send(JSON.stringify({
            action: "start",
            game: this._engine.get_name(),
            player_type: this._player_type,
            type: this._engine.get_type(),
            color: this._engine.current_color(),
            player_color: this._color,
            opponent_color: this._opponent_color
        }));
        await waitUntil(() => this._started);
    }

}

export default {
    RestWebServicePlayer: RestWebServicePlayer
};
