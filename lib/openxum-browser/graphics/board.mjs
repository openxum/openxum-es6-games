"use strict";


const draw_board_with_cells = function(context, offsetX, offsetY, deltaX, deltaY, column_number, line_number, stroke_color, fill_color) {
    context.lineWidth = 1;
    context.strokeStyle = stroke_color;
    context.fillStyle = fill_color;
    for (let i = 0; i < column_number; ++i) {
        for (let j = 0; j < line_number; ++j) {
            context.beginPath();
            context.moveTo(offsetX + i * deltaX, offsetY + j * deltaY);
            context.lineTo(offsetX + (i + 1) * deltaX - 2, offsetY + j * deltaY);
            context.lineTo(offsetX + (i + 1) * deltaX - 2, offsetY + (j + 1) * deltaY - 2);
            context.lineTo(offsetX + i * deltaX, offsetY + (j + 1) * deltaY - 2);
            context.moveTo(offsetX + i * deltaX, offsetY + j * deltaY);
            context.closePath();
            context.fill();
        }
    }
}
const draw_round_rect = function (context, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke === "undefined") {
        stroke = true;
    }
    if (typeof radius === "undefined") {
        radius = 5;
    }
    context.beginPath();
    context.moveTo(x + radius, y);
    context.lineTo(x + width - radius, y);
    context.quadraticCurveTo(x + width, y, x + width, y + radius);
    context.lineTo(x + width, y + height - radius);
    context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    context.lineTo(x + radius, y + height);
    context.quadraticCurveTo(x, y + height, x, y + height - radius);
    context.lineTo(x, y + radius);
    context.quadraticCurveTo(x, y, x + radius, y);
    context.closePath();
    if (stroke) {
        context.stroke();
    }
    if (fill) {
        context.fill();
    }
};

export default {
    draw_round_rect: draw_round_rect,
    draw_board_with_cells: draw_board_with_cells
};
