"use strict";

let draw_button = function(context, pos_x, pos_y, text_width, text) {
    const text_height = 23;

    context.lineWidth = 1;
    context.strokeStyle = "#000000";
    context.fillStyle = "#757D75";

    context.beginPath();
    context.moveTo(pos_x, pos_y);
    context.lineTo(pos_x + text_width, pos_y);
    context.lineTo(pos_x + text_width, pos_y + text_height);
    context.lineTo(pos_x, pos_y + text_height);
    context.lineTo(pos_x, pos_y);
    context.closePath();
    context.fill();
    context.stroke();

    context.fillStyle = "#ffffff";
    context.font = "20px Colibri";
    context.textAlign = "center";
    context.textBaseline = "top";
    context.beginPath();
    context.fillText(text, pos_x + text_width / 2, pos_y);
    context.closePath();
    context.fill();
}

export default {
    draw_button: draw_button
};