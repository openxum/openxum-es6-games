import board from "./board.mjs";
import button from "./button.mjs";
import marble from "./marble.mjs";
import ring from "./ring.mjs";

export default {
    board: board,
    button: button,
    marble: marble,
    ring: ring
};
