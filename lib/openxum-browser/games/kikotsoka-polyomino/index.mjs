"use strict";

import KikotsokaPolyomino from '../../../openxum-core/games/kikotsoka-polyomino/index.mjs';
import AI from '../../../openxum-ai/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            random: AI.Generic.RandomPlayer,
            mcts: AI.Generic.RandomPlayer // AI.Specific.KikotsokaPolyomino.MCTSPlayer
        },
        colors: {
            first: KikotsokaPolyomino.Color.BLACK,
            init: KikotsokaPolyomino.Color.BLACK,
            list: [
                {key: KikotsokaPolyomino.Color.BLACK, value: 'black'},
                {key: KikotsokaPolyomino.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: KikotsokaPolyomino.GameType.SMALL,
            list: [
                {key: KikotsokaPolyomino.GameType.SMALL, value: 'small'},
                {key: KikotsokaPolyomino.GameType.MEDIUM, value: 'medium'},
                {key: KikotsokaPolyomino.GameType.LARGE, value: 'large'}
            ]
        },
        opponent_color(color) {
            return color === KikotsokaPolyomino.Color.BLACK ? KikotsokaPolyomino.Color.WHITE : KikotsokaPolyomino.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
