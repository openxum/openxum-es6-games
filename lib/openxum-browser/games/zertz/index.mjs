"use strict";

import Zertz from '../../../openxum-core/games/zertz/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer // TODO: MCTSPlayer
        },
        colors: {
            first: Zertz.Color.ONE,
            init: Zertz.Color.ONE,
            list: [
                {key: Zertz.Color.ONE, value: 'black'},
                {key: Zertz.Color.TWO, value: 'white'}
            ]
        },
        modes: {
            init: Zertz.GameType.REGULAR,
            list: [
                {key: Zertz.GameType.BLITZ, value: 'blitz'},
                {key: Zertz.GameType.REGULAR, value: 'regular'}
            ]
        },
        opponent_color(color) {
            return color === Zertz.Color.ONE ? Zertz.Color.TWO : Zertz.Color.ONE;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
