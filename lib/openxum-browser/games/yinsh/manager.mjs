"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Yinsh from '../../../openxum-core/games/yinsh/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);
        this.that(this);
    }

    build_move() {
        return new Yinsh.Move();
    }

    get_current_color() {
        return this.engine().current_color() === Yinsh.Color.BLACK ? 'Black' : 'White';
    }

    static get_name() {
        return 'yinsh';
    }

    get_winner_color() {
        return this.engine().winner_is() === Yinsh.Color.BLACK ? 'black' : 'white';
    }

    process_move() {
    }
}

export default Manager;
