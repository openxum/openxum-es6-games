"use strict";

import komivoki from '../../../openxum-core/games/komivoki/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
      ai: {
        random: AI.RandomPlayer,
        alphabeta: AI.AlphaBetaPlayer,
        mcts  : AI.MCTSPlayer
      },
      colors: {
        first: komivoki.Color.BLACK,
        init: komivoki.Color.BLACK,
        list: [
          {key: komivoki.Color.BLACK, value: 'black'},
          {key: komivoki.Color.WHITE, value: 'white'}
        ]
      },
      modes: {
        init: komivoki.GameType.VARIANT_1,
        list: [
          {key: komivoki.GameType.VARIANT_1, value: 'variant 1'},
          {key: komivoki.GameType.VARIANT_2, value: 'variant 2'}
        ]
      },
      opponent_color(color) {
        return color === komivoki.Color.BLACK ? komivoki.Color.WHITE : komivoki.Color.BLACK;
      },
      types: {
        init: 'ai',
        list: [
          {key: 'gui', value: 'GUI'},
          {key: 'ai', value: 'AI'},
          {key: 'online', value: 'Online'},
          {key: 'offline', value: 'Offline'}
        ]
      }
    }
  };
  