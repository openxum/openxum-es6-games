"use strict";

import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';
import Komivoki from '../../../openxum-core/games/Komivoki/index.mjs';
import Phase from '../../../openxum-core/games/Komivoki/phase.mjs';

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._deltaX = 0;
        this._deltaY = 0;
        this._offsetX = 0;
        this._offsetY = 0;
        this._move = null;
        this._selected_piece = null;
    }

    draw() {
        this._context.clearRect(0, 0, this._canvas.height, this._canvas.width)
        this._context.lineWidth = 2;
        this._context.fillStyle = "#6e260e";

        Graphics.board.draw_round_rect(this._context, 10, 10, this._canvas.width - 20, this._canvas.height - 20,
            17, true, true);
        Graphics.board.draw_board_with_cells(this._context, this._offsetX, this._offsetY, this._deltaX, this._deltaY,
            this._engine._size[0], this._engine._size[1], "#000000", "#e5ded2");
        this._draw_pawns();

        if (this._engine._phase === Komivoki.Phase.MOVE_ENTITY) {
            if (this._selected_piece) {
                this._draw_possible_move_for_selected();
            }
        } else if (this._engine._phase === Komivoki.Phase.CAPTURE_ENTITY_BEFORE || this._engine._phase === Komivoki.Phase.CAPTURE_ENTITY_AFTER) {
            this._draw_possible_captures();
        } else {
            const possible_moves = this._engine.get_possible_move_list();

            if (possible_moves.length === 1 && possible_moves[0]._type === Komivoki.MoveType.PASS) {
                this._draw_pass_button();
            }
        }
    }

    get_move() {
        if (this._move) {
            return this._move;
        } else {
            return new Komivoki.Move(Komivoki.MoveType.PASS);
        }
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move) {
        this._manager.play();
        // TODO !!!!!
    }

    reset() {
        this._move = null;
        this._selected_piece = null;
    }

    unselect() {
        this._selected_piece = null;
    }

    set_canvas(c) {
        super.set_canvas(c);
        this._canvas.addEventListener("click", (e) => {
            let pos = this._get_click_position(e);

            if (pos.x >= 0 && pos.x < this._engine._size[0] && pos.y >= 0 && pos.y < this._engine._size[1]) {
                this._on_click(pos);
            }
        });
        this._deltaX = (this._canvas.width * 0.95 - 40) / this._engine._size[0];
        this._deltaY = (this._canvas.height * 0.95 - 40) / this._engine._size[1] - 7;
        this._offsetX = this._canvas.width / 2 - this._deltaX * (this._engine._size[0] / 2);
        this._offsetY = this._canvas.height / 2 - this._deltaY * (this._engine._size[1] / 2);
        this.draw();
    }

    _draw_possible_captures() {
        let list_for_selected = this._engine.get_possible_move_list();

        list_for_selected.forEach(move => {
            if (move._type === Komivoki.MoveType.CAPTURE) {
                const to_element = this._engine._board[move._from._column + (move._from._line * this._engine._size[0])];
                const column = move._from._column;
                const line = move._from._line;

                this._draw_selected_stack(to_element.length, line, column, "#ff0000");
            }
        });
    }

    _draw_possible_move_for_selected() {
        if (this._selected_piece) {
            let list_for_selected = this._possible_move();

            list_for_selected.forEach(move => {
                if (move._type === Komivoki.MoveType.MOVE) {
                    const to_element = this._engine._board[move._to._column + (move._to._line * this._engine._size[0])];
                    const from_element = this._engine._board[move._from._column + (move._from._line * this._engine._size[0])];

                    this._draw_pawn(from_element[0]._color, move._to._column, move._to._line, this._engine._max_length - to_element.length - 1);
                }
            });
        }
    }

    _draw_pass_button() {
        const text_width = 100;
        const pos_x = this._offsetX + Math.floor((this._width - 2 * this._offsetX - text_width) / 2);
        const pos_y = this._offsetY + Math.floor(this._height - 2 * this._offsetY + 5);

        Graphics.button.draw_button(this._context, pos_x, pos_y, text_width, "Pass");
    }

    _draw_pawn(color, column, line, index, level = 0) {
        const spawn_height = this._deltaY / this._engine._max_length - 1;
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#880000";
        this._context.fillStyle = level === 0 ? "#ffffff": (color === Komivoki.Color.WHITE ? "#B9B1A2" : "#744460");
        this._context.beginPath();
        this._context.moveTo(this._offsetX + column * this._deltaX + 2, this._offsetY + line * this._deltaY + index * spawn_height + 2);
        this._context.lineTo(this._offsetX + (column + 1) * this._deltaX - 4,  this._offsetY + line * this._deltaY + index * spawn_height + 2);
        this._context.lineTo(this._offsetX + (column + 1) * this._deltaX - 4, this._offsetY + line * this._deltaY + (index + 1) * spawn_height - 2);
        this._context.lineTo(this._offsetX + column * this._deltaX + 2, this._offsetY + line * this._deltaY + (index + 1) * spawn_height - 2);
        this._context.closePath();
        this._context.fill();

        this._context.fillStyle = color === Komivoki.Color.WHITE ? "#3E3E3C" : "#A0AABB";
        for (let q = 1; q <= level; q++) {
            let x = this._offsetX + (column + q / (level + 1)) * this._deltaX - 2;
            let y = this._offsetY + line * this._deltaY + (index + 0.5) * spawn_height;

            this._context.beginPath();
            this._context.arc(x, y, 5, 0.0, 2 * Math.PI);
            this._context.closePath();
            this._context.fill();
        }
    }

    _draw_pawns() {
        for (let column = 0; column < this._engine._size[0]; ++column) {
            for (let line = 0; line < this._engine._size[1]; ++line) {
                const element = this._engine._board[column + line * this._engine._size[0]];

                if (element.length > 0) {
                    let index = this._engine._max_length - 1;

                    element.forEach(entity => {
                        this._draw_pawn(element[0]._color, column, line, index, entity._level);
                        index--
                    });
                    if (this._selected_piece && this._selected_piece._column === column && this._selected_piece._line === line) {
                        this._draw_selected_stack(element.length, line, column, "#e1e145");
                    }
                }
            }
        }
    }

    _draw_selected_stack(size, line, column, color) {
        const spawn_height = this._deltaY / this._engine._max_length - 1;
        const index = this._engine._max_length - size;
        const y = this._offsetY + line * this._deltaY + index * spawn_height + 2;

        this._context.strokeStyle = color;
        this._context.lineWidth = 2;
        this._context.beginPath();
        this._context.moveTo(this._offsetX + column * this._deltaX + 2, y);
        this._context.lineTo(this._offsetX + (column + 1) * this._deltaX - 4, y);
        this._context.lineTo(this._offsetX + (column + 1) * this._deltaX - 4, this._offsetY + (line + 1) * this._deltaY - 4);
        this._context.lineTo(this._offsetX + column * this._deltaX + 2, this._offsetY + (line + 1) * this._deltaY - 4);
        this._context.lineTo(this._offsetX + column * this._deltaX + 2, y);
        this._context.closePath();
        this._context.stroke();
    }

    _get_click_position(e) {
        const rect = this._canvas.getBoundingClientRect();
        const x = (e.clientX - rect.left) * this._scaleX - this._offsetX;
        const y = (e.clientY - rect.top) * this._scaleY - this._offsetY;

        return {x: Math.floor(x / this._deltaX), y: Math.floor(y / (this._deltaX - 7))};
    }

    _on_click(pos) {
        const index = pos.x + pos.y * this._engine._size[0];

        if (!this._engine.is_finished()) {
            const phase = this._engine._phase;

            if (phase === Komivoki.Phase.MOVE_ENTITY) {
                if (this._selected_piece && this._engine._board[index].length > 0 && pos.x === this._selected_piece._column && pos.y === this._selected_piece._line) {
                    this.unselect();
                    this.draw();
                } else if (!this._selected_piece && this._engine._board[index].length > 0 && this._engine._board[index][0]._color === this._engine.current_color()) {
                    this._selected_piece = new Komivoki.Coordinates(pos.x, pos.y);
                    this.draw();
                } else {
                    this._on_click_move(pos);
                }
            } else if (phase === Komivoki.Phase.CAPTURE_ENTITY_BEFORE || phase === Komivoki.Phase.CAPTURE_ENTITY_AFTER) {
                this._on_click_capture(pos);
            } else { // PASS?
                const text_width = 100;
                const pos_x = (this._width - text_width) / 2;

                if (pos.x > pos_x && pos.x < pos_x + text_width) {
                    this._manager.play();
                }
            }
        }
    }

    _on_click_capture(pos) {
        const list_for_selected = this._engine.get_possible_move_list();
        let found = false;
        let index = 0;

        this._move = null;
        while (!found && index < list_for_selected.length) {
            const element = list_for_selected[index];

            if (element._from._column === pos.x && element._from._line === pos.y) {
                this._move = element;
                found = true;
            } else {
                ++index;
            }
        }
        if (found) {
            this.unselect();
            this._manager.play();
        }
    }

    _on_click_move(pos) {
        if (this._selected_piece) {
            const list_for_selected = this._possible_move();
            let found = false;
            let index = 0;

            this._move = null;
            while (!found && index < list_for_selected.length) {
                const element = list_for_selected[index];

                if (element._to._column === pos.x && element._to._line === pos.y) {
                    if (this._selected_piece._column === element._from._column && this._selected_piece._line === element._from._line) {
                        this._move = element;
                        found = true;
                    }
                }
                ++index;
            }
            if (found) {
                this.unselect();
                this._manager.play();
            }
        }
    }

    _possible_move() {
        if (this._selected_piece) {
            let list = this._engine.get_possible_move_list();
            let list_for_selected = [];

            if (this._engine._phase !== Phase.CAPTURE_ENTITY_BEFORE) {
                list.forEach(element => {
                    if (element._type === 0) {
                        if (element._from._column === this._selected_piece._column && element._from._line === this._selected_piece._line) {
                            list_for_selected.push(element);
                        }
                    } else {
                        list_for_selected.push(element)
                    }
                });
            } else {
                return list;
            }
            return list_for_selected;
        }
    }
}

export default Gui;