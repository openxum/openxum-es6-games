"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Komivoki from '../../../openxum-core/games/komivoki/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
      super(t, e, g, o, s, w, f);
      this.that(this);
    }

    build_move() {
        return new Komivoki.Move(Komivoki.MoveType.PASS);
    }

    get_current_color() {
        return this._engine.current_color() === Komivoki.Color.WHITE ? 'White' : 'Black';
    }

    get_name() {
        return 'komivoki';
    }

    get_winner_color() {
        return this.engine().winner_is() === Komivoki.Color.WHITE ? 'white' : 'black';
    }

    process_move() { }
}

export default Manager;
