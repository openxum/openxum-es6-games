require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let victoire = 0;

for (let i = 0; i < 20; i += 1) {
    let e = new core.Tintas.Engine(core.Tintas.GameType.STANDARD, core.Tintas.Color.PLAYER_1);
    let p1 = new ai.Generic.RandomPlayer(core.Tintas.Color.PLAYER_1, core.Tintas.Color.PLAYER_2, e);
    let p2 = new ai.Generic.RandomPlayer(core.Tintas.Color.PLAYER_2, core.Tintas.Color.PLAYER_1, e);
    let p = p1;
    let moves = [];
    while (!e.is_finished()) {
        let move = p.move();

        moves.push(move);
        e.move(move);
        p = p === p1 ? p2 : p1;
    }
    console.log(e.winner_is());
    if (e.winner_is() === 0) {
        victoire += 1;
    }
}

console.log("victoire", victoire);
