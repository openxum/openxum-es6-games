require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let e = new core.Zertz.Engine(core.Zertz.GameType.REGULAR, core.Zertz.Color.ONE);
let p1 = new ai.Generic.RandomPlayer(core.Zertz.Color.ONE, core.Zertz.Color.TWO, e);
let p2 = new ai.Generic.RandomPlayer(core.Zertz.Color.TWO, core.Zertz.Color.ONE, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
    let move = p.move();

    if (move.constructor === Array) {
        for (let i = 0; i < move.length; ++i) {
            console.log(move[i].to_string());
        }
    } else {
        console.log(move.to_string());
    }

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === core.Zertz.Color.ONE ? "one" : "two"));
for (let index = 0; index < moves.length; ++index) {
    console.log(moves[index].to_string());
}
