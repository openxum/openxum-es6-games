require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let win = [0, 0];
for (let i = 0; i < 10; ++i) {
    let e = new core.Invers.Engine(core.Invers.GameType.STANDARD, core.Invers.Color.RED);
    let p1 = new ai.Specific.Invers.MCTSPlayer(core.Invers.Color.RED, core.Invers.Color.YELLOW, e);
    let p2 = new ai.Generic.RandomPlayer(core.Invers.Color.YELLOW, core.Invers.Color.RED, e);
    let p = p1;

    while (!e.is_finished()) {
        let move = p.move();

        e.move(move);
        p = p === p1 ? p2 : p1;
    }
    ++win[e.winner_is()];
}

console.log("Random: " + win[core.Invers.Color.YELLOW] + " wins");
console.log("MCTS: " + win[core.Invers.Color.RED] + " wins");
